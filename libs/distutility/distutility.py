#-------------------------------------------------------------------------------
#  \author Jan Piotr Buchmann <jpb@members.fsf.org>
#  \copyright 2020
#  \version 0.1.0
#-------------------------------------------------------------------------------


import json
import subprocess


def run_gitcmd(git_cmd):
  """Run git commands"""
  output = subprocess.run(git_cmd, text=True, capture_output=True)
  if output.returncode == 0:
    return output.stdout.strip()
  return None

def is_valid_git():
  """Test if git is installed and if we are in a git repository"""
  if not run_gitcmd(['git', 'rev-parse', '--is-inside-work-tree']):
    return False
  return True

def git_root():
  """Return root of current git repository"""
  groot =  run_gitcmd(['git', 'rev-parse', '--show-toplevel'])
  if not groot:
    return None
  return groot

def git_revision():
  """Get git revision as short SHA1 string"""
  revision = run_gitcmd(['git', 'rev-parse', '--short', 'HEAD'])
  if not revision:
    return 'UNK'
  return revision

def git_branch():
  """Get git branch from where setup.py was run."""
  branch = run_gitcmd(['git', 'rev-parse', '--abbrev-ref', 'HEAD'])
  if not branch:
    return 'UNK'
  return branch

def semver_string(major, minor, micro):
    return "{0:d}.{1:d}.{2:d}".format(major, minor, micro)

def git_commits_since_last_tag():
  """Count commits since last tag on actual branch"""
  last_tag = run_gitcmd(['git', 'rev-list', '--abbrev-commit', '--tags', '--no-walk', '--max-count=1'])
  if not last_tag:
    return int(run_gitcmd(['git', 'rev-list', 'HEAD', '--count']))
  return int(run_gitcmd(['git', 'rev-list', last_tag+'..HEAD', '--count']))

def assemble_version(baseversion):
  """Assemble version with all attributes"""
  if is_valid_git():
    baseversion.update({'commits':git_commits_since_last_tag(),
                        'branch':git_branch(),
                        'revision':git_revision()})
  return baseversion

def read_json_versionfile(verfil):
  fh = open(verfil, 'r')
  version = json.load(fh)
  fh.close()
  return version

def repo_version(versionfile):
  """Construct repo version string"""
  version = assemble_version(read_json_versionfile(versionfile))
  rver = semver_string(version['major'], version['minor'], version['micro'])
  if 'revision' in version:
    return "{}+{}".format(rver, version['revision'])
  return rver

def pypi_version(versionfile, main_branch='master'):
  """Construct valid PyPi version string"""
  version = assemble_version(read_json_versionfile(versionfile))
  pypiv = semver_string(version['major'], version['minor'], version['micro'])
  if 'branch' in version:
    if version['branch'] == main_branch:
      if version['commits'] > 0:
        return "{}.{:d}".format(pypiv, version['commits'])
      return "{}".format(pypiv)
    return "{}.{}{:d}".format(pypiv, version['branch'], version['commits'])
  return pypiv
