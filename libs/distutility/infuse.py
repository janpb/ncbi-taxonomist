#!/usr/bin/env python3
"""
..
  Copyright 2020 The University of Sydney

  A simple cli for the distutility functions to prepare a repo for a Pypi
  release. It adds/infuses VERSION files for preapring tools via setuptool and
  setup.py.

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


import io
import os
import sys
import argparse

import distutility

class Infuser:

  def __init__(self, version_file, main_branch):
    self.groot = distutility.git_root()
    if not self.groot:
      sys.exit("Not a git repositoy. Abort")
    print("git repo: {}".format(self.groot), file=sys.stderr)
    self.vfile = version_file
    self.main_branch = main_branch
    self.obsolete_paths = []
    self.version_file = "VERSION"
    self.pypi_version_file = self.version_file+".pypi"


  def add_distrib_versionfile(self, path, dryrun=False):
    if not os.path.exists(os.path.relpath(os.path.dirname(path), self.groot)):
      sys.exit("Distribution file {} does not exists. Abort".format(os.path.abspath(path)))
    versionfile = os.path.abspath(os.path.join(path, self.version_file))
    ver = distutility.repo_version(self.vfile)
    if dryrun:
      print("Dryrun: Write version {} to {}".format(ver, versionfile), file=sys.stderr)
      self.obsolete_paths.append(versionfile)
      return None
    fh = open(versionfile, 'w')
    fh.write(ver+'\n')
    fh.close()
    self.obsolete_paths.append(os.path.abspath(versionfile))
    print("Distribution (Update MANIFEST.in if required):\n\tversion:{}\n\tfile:{}".format(ver,
          versionfile), file=sys.stderr)

  def write_pypi_version(self, path, dryrun=False):
    """Read this file in setup.py"""
    if not os.path.exists(os.path.relpath(os.path.dirname(path), self.groot)):
      sys.exit("Distribution file {} does not exists. Abort".format(os.path.abspath(path)))
    pypivfile = os.path.join(os.path.abspath(path), self.pypi_version_file)
    ver = distutility.pypi_version(self.vfile, self.main_branch)
    if dryrun:
      print("Dryrun: write PyPi version {} to {}".format(ver, pypivfile), file=sys.stderr)
      self.obsolete_paths.append(pypivfile)
      return None
    fh = open(pypivfile, 'w')
    fh.write(ver+'\n')
    fh.close()
    self.obsolete_paths.append(os.path.join(os.path.abspath(pypivfile), pypivfile))
    print("PyPi:\n\tversion:{}\n\tfile:{}.".format(ver, pypivfile), file=sys.stderr)


def main():
  ap = argparse.ArgumentParser(usage='infuse.py [<args>]')
  ap.add_argument('-f', '--baseversionfile',
                  type=str,
                  required=True,
                  metavar='FILE',
                  help='Path to version file')
  ap.add_argument('-p', '--pypiversionfile',
                  type=str,
                  required=True,
                  metavar='PATH',
                  help='Path to write Pypi version file')
  ap.add_argument('-d', '--versionfile',
                  type=str,
                  required=True,
                  metavar='PATH',
                  help='Path to write version file')
  ap.add_argument('-b', '--mainbranch',
                  type=str,
                  default='master',
                  metavar='NAME',
                  help='name of main git branch to exclude from Pypi version')
  ap.add_argument('-n', '--dryrun',
                  action='store_true',
                  help='Dry run , do not write files')
  args = ap.parse_args()

  inf = Infuser(args.baseversionfile, args.mainbranch)
  inf.add_distrib_versionfile(args.versionfile, dryrun=args.dryrun)
  inf.write_pypi_version(args.pypiversionfile, dryrun=args.dryrun)
  print("Obsolete paths after running setup.py and upload to Pypi:", file=sys.stderr)
  for i in inf.obsolete_paths:
    print("\t{}".format(i), file=sys.stderr)
  return 0

if __name__ == '__main__':
  main()
