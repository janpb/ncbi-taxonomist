.. _resolvecmd:

Resolve
=======

The ``resolve`` command resolve lineages. Names and taxid can be resolved
directly, while accessions need a mapping step first.

Taxids and names
----------------

.. code-block:: shell

  ncbi-taxonomist  resolve -n man -t 2

Accessions
----------

.. code-block:: shell

  $: ncbi-taxonomist map -a QZWG01000002.1 MG831203  | ncbi-taxonomist resolve -m

Output format
-------------

The result shows the used command, query, type of result, and the
corresponding lineage. In case of queried names or taxids, the data for the
taxon used as query is shown. For accessions, the queried accession data is
shown.

JSON output
+++++++++++

Single mapping result
~~~~~~~~~~~~~~~~~~~~~

.. code-block:: json

  {
    "mode" : "resolve",
    "query" : "man",
    "cast" : "taxon",
    "parentid" : 9605,
    "name":"Homo sapiens",
    "taxon" :
    {
      "taxid" : 9606,
      "rank" : "species",
      "names" :
      {
        "Homo sapiens" : "scientific_name",
        "human" : "GenbankCommonName",
        "man" : "CommonName"
      }
    },
    "lineage":
    [
      {"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},
      {"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"},
      {"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"},
      {"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"},
      {"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"},
      {"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"},
      {"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"},
      {"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"},
      {"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"},
      {"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"},
      {"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"},
      {"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"},
      {"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"},
      {"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"},
      {"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"},
      {"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"},
      {"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"},
      {"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"},
      {"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"},
      {"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"},
      {"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"},
      {"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"},
      {"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"},
      {"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"},
      {"taxid":33511,"rank":"clade","names":{"Deuterostomia":"scientific_name"},"parentid":33213,"name":"Deuterostomia"},
      {"taxid":33213,"rank":"clade","names":{"Bilateria":"scientific_name"},"parentid":6072,"name":"Bilateria"},
      {"taxid":6072,"rank":"clade","names":{"Eumetazoa":"scientific_name"},"parentid":33208,"name":"Eumetazoa"},
      {"taxid":33208,"rank":"kingdom","names":{"Metazoa":"scientific_name"},"parentid":33154,"name":"Metazoa"},
      {"taxid":33154,"rank":"clade","names":{"Opisthokonta":"scientific_name"},"parentid":2759,"name":"Opisthokonta"},
      {"taxid":2759,"rank":"superkingdom","names":{"Eukaryota":"scientific_name"},"parentid":131567,"name":"Eukaryota"},
      {"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}
    ]
  }

Multiple mapping results
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: json

  {"mode":"resolve","query":"man","cast":"taxon","taxon":{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},"lineage":[{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},{"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"},{"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"},{"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"},{"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"},{"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"},{"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"},{"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"},{"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"},{"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"},{"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"},{"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"},{"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"},{"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"},{"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"},{"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"},{"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"},{"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"},{"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"},{"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"},{"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"},{"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"},{"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"},{"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"},{"taxid":33511,"rank":"clade","names":{"Deuterostomia":"scientific_name"},"parentid":33213,"name":"Deuterostomia"},{"taxid":33213,"rank":"clade","names":{"Bilateria":"scientific_name"},"parentid":6072,"name":"Bilateria"},{"taxid":6072,"rank":"clade","names":{"Eumetazoa":"scientific_name"},"parentid":33208,"name":"Eumetazoa"},{"taxid":33208,"rank":"kingdom","names":{"Metazoa":"scientific_name"},"parentid":33154,"name":"Metazoa"},{"taxid":33154,"rank":"clade","names":{"Opisthokonta":"scientific_name"},"parentid":2759,"name":"Opisthokonta"},{"taxid":2759,"rank":"superkingdom","names":{"Eukaryota":"scientific_name"},"parentid":131567,"name":"Eukaryota"},{"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}]}
  {"mode":"resolve","query":"2","cast":"taxon","taxon":{"taxid":2,"rank":"superkingdom","names":{"Bacteria":"scientific_name","eubacteria":"GenbankCommonName","bacteria":"BlastName","Monera":"Inpart","Procaryotae":"Inpart","Prokaryota":"Inpart","Prokaryotae":"Inpart","prokaryote":"Inpart","prokaryotes":"Inpart"},"parentid":131567,"name":"Bacteria"},"lineage":[{"taxid":2,"rank":"superkingdom","names":{"Bacteria":"scientific_name","eubacteria":"GenbankCommonName","bacteria":"BlastName","Monera":"Inpart","Procaryotae":"Inpart","Prokaryota":"Inpart","Prokaryotae":"Inpart","prokaryote":"Inpart","prokaryotes":"Inpart"},"parentid":131567,"name":"Bacteria"},{"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}]}
  {"mode":"resolve","query":"MG831203","cast":"accs","accs":{"taxid":198112,"accessions":{"accessionversion":"MG831203.1","caption":"MG831203","extra":"gi|1496532032|gb|MG831203.1|"},"db":"nucleotide","uid":1496532032},"lineage":[{"taxid":198112,"rank":"species","names":{"Deformed wing virus":"scientific_name","DWV":"GenbankAcronym"},"parentid":232799,"name":"Deformed wing virus"},{"taxid":232799,"rank":"genus","names":{"Iflavirus":"scientific_name"},"parentid":699189,"name":"Iflavirus"},{"taxid":699189,"rank":"family","names":{"Iflaviridae":"scientific_name"},"parentid":464095,"name":"Iflaviridae"},{"taxid":464095,"rank":"order","names":{"Picornavirales":"scientific_name"},"parentid":2732506,"name":"Picornavirales"},{"taxid":2732506,"rank":"class","names":{"Pisoniviricetes":"scientific_name"},"parentid":2732408,"name":"Pisoniviricetes"},{"taxid":2732408,"rank":"phylum","names":{"Pisuviricota":"scientific_name"},"parentid":2732396,"name":"Pisuviricota"},{"taxid":2732396,"rank":"kingdom","names":{"Orthornavirae":"scientific_name"},"parentid":2559587,"name":"Orthornavirae"},{"taxid":2559587,"rank":"clade","names":{"Riboviria":"scientific_name"},"parentid":10239,"name":"Riboviria"},{"taxid":10239,"rank":"superkingdom","names":{"Viruses":"scientific_name"},"parentid":null,"name":"Viruses"}]}

XML output
++++++++++

Single mapping result
~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml

  <resolve>
  <query value="MG831203" cast="accession">
    <accession>
      <taxid>198112</taxid>
      <uid>1496532032</uid>
      <database>nucleotide</database>
      <accessions>
        <accessionversion>MG831203.1</accessionversion>
        <caption>MG831203</caption>
        <extra>gi|1496532032|gb|MG831203.1|</extra>
      </accessions>
    </accession>
  </query>
  <lineage>
    <taxon>
      <taxid>198112</taxid>
      <rank>species</rank>
      <name>Deformed wing virus</name>
      <parentid>232799</parentid>
      <names>
        <name type="scientific_name">Deformed wing virus</name>
        <name type="GenbankAcronym">DWV</name>
      </names>
    </taxon>
    <taxon>
      <taxid>232799</taxid>
      <rank>genus</rank>
      <name>Iflavirus</name>
      <parentid>699189</parentid>
      <names>
        <name type="scientific_name">Iflavirus</name>
      </names>
    </taxon>
    <taxon>
      <taxid>699189</taxid>
      <rank>family</rank>
      <name>Iflaviridae</name>
      <parentid>464095</parentid>
      <names>
        <name type="scientific_name">Iflaviridae</name>
      </names>
    </taxon>
    <taxon>
      <taxid>464095</taxid>
      <rank>order</rank>
      <name>Picornavirales</name>
      <parentid>2732506</parentid>
      <names>
        <name type="scientific_name">Picornavirales</name>
      </names>
    </taxon>
    <taxon>
      <taxid>2732506</taxid>
      <rank>class</rank>
      <name>Pisoniviricetes</name>
      <parentid>2732408</parentid>
      <names>
        <name type="scientific_name">Pisoniviricetes</name>
      </names>
    </taxon>
    <taxon>
      <taxid>2732408</taxid>
      <rank>phylum</rank>
      <name>Pisuviricota</name>
      <parentid>2732396</parentid>
      <names>
        <name type="scientific_name">Pisuviricota</name>
      </names>
    </taxon>
    <taxon>
      <taxid>2732396</taxid>
      <rank>kingdom</rank>
      <name>Orthornavirae</name>
      <parentid>2559587</parentid>
      <names>
        <name type="scientific_name">Orthornavirae</name>
      </names>
    </taxon>
    <taxon>
      <taxid>2559587</taxid>
      <rank>clade</rank>
      <name>Riboviria</name>
      <parentid>10239</parentid>
      <names>
        <name type="scientific_name">Riboviria</name>
      </names>
    </taxon>
    <taxon>
      <taxid>10239</taxid>
      <rank>superkingdom</rank>
      <name>Viruses</name>
      <parentid>None</parentid>
      <names>
        <name type="scientific_name">Viruses</name>
      </names>
    </taxon>
  </lineage>
  </resolve>

Multiple mapping results
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml

  <resolve><query value="man" cast="name"><taxon><taxid>9606</taxid><rank>species</rank><name>Homo sapiens</name><parentid>9605</parentid><names><name type="scientific_name">Homo sapiens</name><name type="GenbankCommonName">human</name><name type="CommonName">man</name></names></taxon></query><lineage><taxon><taxid>9606</taxid><rank>species</rank><name>Homo sapiens</name><parentid>9605</parentid><names><name type="scientific_name">Homo sapiens</name><name type="GenbankCommonName">human</name><name type="CommonName">man</name></names></taxon><taxon><taxid>9605</taxid><rank>genus</rank><name>Homo</name><parentid>207598</parentid><names><name type="scientific_name">Homo</name></names></taxon><taxon><taxid>207598</taxid><rank>subfamily</rank><name>Homininae</name><parentid>9604</parentid><names><name type="scientific_name">Homininae</name></names></taxon><taxon><taxid>9604</taxid><rank>family</rank><name>Hominidae</name><parentid>314295</parentid><names><name type="scientific_name">Hominidae</name></names></taxon><taxon><taxid>314295</taxid><rank>superfamily</rank><name>Hominoidea</name><parentid>9526</parentid><names><name type="scientific_name">Hominoidea</name></names></taxon><taxon><taxid>9526</taxid><rank>parvorder</rank><name>Catarrhini</name><parentid>314293</parentid><names><name type="scientific_name">Catarrhini</name></names></taxon><taxon><taxid>314293</taxid><rank>infraorder</rank><name>Simiiformes</name><parentid>376913</parentid><names><name type="scientific_name">Simiiformes</name></names></taxon><taxon><taxid>376913</taxid><rank>suborder</rank><name>Haplorrhini</name><parentid>9443</parentid><names><name type="scientific_name">Haplorrhini</name></names></taxon><taxon><taxid>9443</taxid><rank>order</rank><name>Primates</name><parentid>314146</parentid><names><name type="scientific_name">Primates</name></names></taxon><taxon><taxid>314146</taxid><rank>superorder</rank><name>Euarchontoglires</name><parentid>1437010</parentid><names><name type="scientific_name">Euarchontoglires</name></names></taxon><taxon><taxid>1437010</taxid><rank>clade</rank><name>Boreoeutheria</name><parentid>9347</parentid><names><name type="scientific_name">Boreoeutheria</name></names></taxon><taxon><taxid>9347</taxid><rank>clade</rank><name>Eutheria</name><parentid>32525</parentid><names><name type="scientific_name">Eutheria</name></names></taxon><taxon><taxid>32525</taxid><rank>clade</rank><name>Theria</name><parentid>40674</parentid><names><name type="scientific_name">Theria</name></names></taxon><taxon><taxid>40674</taxid><rank>class</rank><name>Mammalia</name><parentid>32524</parentid><names><name type="scientific_name">Mammalia</name></names></taxon><taxon><taxid>32524</taxid><rank>clade</rank><name>Amniota</name><parentid>32523</parentid><names><name type="scientific_name">Amniota</name></names></taxon><taxon><taxid>32523</taxid><rank>clade</rank><name>Tetrapoda</name><parentid>1338369</parentid><names><name type="scientific_name">Tetrapoda</name></names></taxon><taxon><taxid>1338369</taxid><rank>clade</rank><name>Dipnotetrapodomorpha</name><parentid>8287</parentid><names><name type="scientific_name">Dipnotetrapodomorpha</name></names></taxon><taxon><taxid>8287</taxid><rank>superclass</rank><name>Sarcopterygii</name><parentid>117571</parentid><names><name type="scientific_name">Sarcopterygii</name></names></taxon><taxon><taxid>117571</taxid><rank>clade</rank><name>Euteleostomi</name><parentid>117570</parentid><names><name type="scientific_name">Euteleostomi</name></names></taxon><taxon><taxid>117570</taxid><rank>clade</rank><name>Teleostomi</name><parentid>7776</parentid><names><name type="scientific_name">Teleostomi</name></names></taxon><taxon><taxid>7776</taxid><rank>clade</rank><name>Gnathostomata</name><parentid>7742</parentid><names><name type="scientific_name">Gnathostomata</name></names></taxon><taxon><taxid>7742</taxid><rank>clade</rank><name>Vertebrata</name><parentid>89593</parentid><names><name type="scientific_name">Vertebrata</name></names></taxon><taxon><taxid>89593</taxid><rank>subphylum</rank><name>Craniata</name><parentid>7711</parentid><names><name type="scientific_name">Craniata</name></names></taxon><taxon><taxid>7711</taxid><rank>phylum</rank><name>Chordata</name><parentid>33511</parentid><names><name type="scientific_name">Chordata</name></names></taxon><taxon><taxid>33511</taxid><rank>clade</rank><name>Deuterostomia</name><parentid>33213</parentid><names><name type="scientific_name">Deuterostomia</name></names></taxon><taxon><taxid>33213</taxid><rank>clade</rank><name>Bilateria</name><parentid>6072</parentid><names><name type="scientific_name">Bilateria</name></names></taxon><taxon><taxid>6072</taxid><rank>clade</rank><name>Eumetazoa</name><parentid>33208</parentid><names><name type="scientific_name">Eumetazoa</name></names></taxon><taxon><taxid>33208</taxid><rank>kingdom</rank><name>Metazoa</name><parentid>33154</parentid><names><name type="scientific_name">Metazoa</name></names></taxon><taxon><taxid>33154</taxid><rank>clade</rank><name>Opisthokonta</name><parentid>2759</parentid><names><name type="scientific_name">Opisthokonta</name></names></taxon><taxon><taxid>2759</taxid><rank>superkingdom</rank><name>Eukaryota</name><parentid>131567</parentid><names><name type="scientific_name">Eukaryota</name></names></taxon><taxon><taxid>131567</taxid><rank>no rank</rank><name>cellular organisms</name><parentid>None</parentid><names><name type="scientific_name">cellular organisms</name></names></taxon></lineage></resolve>
  <resolve><query value="2" cast="taxid"><taxon><taxid>2</taxid><rank>superkingdom</rank><name>Bacteria</name><parentid>131567</parentid><names><name type="scientific_name">Bacteria</name><name type="GenbankCommonName">eubacteria</name><name type="BlastName">bacteria</name><name type="Inpart">Monera</name><name type="Inpart">Procaryotae</name><name type="Inpart">Prokaryota</name><name type="Inpart">Prokaryotae</name><name type="Inpart">prokaryote</name><name type="Inpart">prokaryotes</name></names></taxon></query><lineage><taxon><taxid>2</taxid><rank>superkingdom</rank><name>Bacteria</name><parentid>131567</parentid><names><name type="scientific_name">Bacteria</name><name type="GenbankCommonName">eubacteria</name><name type="BlastName">bacteria</name><name type="Inpart">Monera</name><name type="Inpart">Procaryotae</name><name type="Inpart">Prokaryota</name><name type="Inpart">Prokaryotae</name><name type="Inpart">prokaryote</name><name type="Inpart">prokaryotes</name></names></taxon><taxon><taxid>131567</taxid><rank>no rank</rank><name>cellular organisms</name><parentid>None</parentid><names><name type="scientific_name">cellular organisms</name></names></taxon></lineage></resolve>
  <resolve><query value="MG831203" cast="accession"><accession><taxid>198112</taxid><uid>1496532032</uid><database>nucleotide</database><accessions><accessionversion>MG831203.1</accessionversion><caption>MG831203</caption><extra>gi|1496532032|gb|MG831203.1|</extra></accessions></accession></query><lineage><taxon><taxid>198112</taxid><rank>species</rank><name>Deformed wing virus</name><parentid>232799</parentid><names><name type="scientific_name">Deformed wing virus</name><name type="GenbankAcronym">DWV</name></names></taxon><taxon><taxid>232799</taxid><rank>genus</rank><name>Iflavirus</name><parentid>699189</parentid><names><name type="scientific_name">Iflavirus</name></names></taxon><taxon><taxid>699189</taxid><rank>family</rank><name>Iflaviridae</name><parentid>464095</parentid><names><name type="scientific_name">Iflaviridae</name></names></taxon><taxon><taxid>464095</taxid><rank>order</rank><name>Picornavirales</name><parentid>2732506</parentid><names><name type="scientific_name">Picornavirales</name></names></taxon><taxon><taxid>2732506</taxid><rank>class</rank><name>Pisoniviricetes</name><parentid>2732408</parentid><names><name type="scientific_name">Pisoniviricetes</name></names></taxon><taxon><taxid>2732408</taxid><rank>phylum</rank><name>Pisuviricota</name><parentid>2732396</parentid><names><name type="scientific_name">Pisuviricota</name></names></taxon><taxon><taxid>2732396</taxid><rank>kingdom</rank><name>Orthornavirae</name><parentid>2559587</parentid><names><name type="scientific_name">Orthornavirae</name></names></taxon><taxon><taxid>2559587</taxid><rank>clade</rank><name>Riboviria</name><parentid>10239</parentid><names><name type="scientific_name">Riboviria</name></names></taxon><taxon><taxid>10239</taxid><rank>superkingdom</rank><name>Viruses</name><parentid>None</parentid><names><name type="scientific_name">Viruses</name></names></taxon></lineage></resolve>
