.. _mapcmd:

Map
===

The ``map`` command maps taxonomic information for taxids, names, and
accessions. Without specifing the ``-edb`` argument, ``nucleotide`` Entrez
database is assumed.

Taxids and names
----------------

Taxids and names can be mapped together. The taxids and names can be separated
by commas and/or space. However, names containing space need to be encapsulated
by ``'``. For example:

..  code-block:: shell

  $: ncbi-taxonomist map -t 562, 10508 -n man 'Influenza B virus (B/Acre/121609/2012)', chimpanzee


Mapping accession
-----------------
The default database to map accessions is nucleotide. To map an accession form a
different database, it has to be specified by the --entrezdb/-edb argument.


Supported access Entrez databases
---------------------------------

===============   =======
Entrez database   Example
===============   =======
assembly          ``ncbi-taxonomist map -edb assembly -a ASM1001476v1 ViralProj177933``
bioproject        ``ncbi-taxonomist map -edb bioproject -a PRJNA604394``
nucleotide        ``ncbi-taxonomist map -edb nucleotide -a MH510449.1``

                  ``ncbi-taxonomist map -a MH510449.1``
protein           ``ncbi-taxonomist map -a YP_009345145 -edb protein``
===============   =======

.. note::
  Querying the following databases does not return the queried accession in the
  results. Therefore, results cannot identify which accession corresponds to
  which results if more than one are requested. To solve the one-to-one
  relationship, each of the accessions from these databases needs to be queried
  one-by-one and not as batch query. Future releases will try to implement such
  queries.

 - biosample
 - biosystems
 - cdd
 - dbvar
 - gap
 - gapplus
 - gene
 - genome
 - geoprofiles: using accessions like ``GDS6063`` should work
 - proteinclusters: ``commontaxonomy`` attribute can be used as name
 - sra: Only XML results. Needs a dedicated parser



Output format
-------------

The result shows the used command, query, type of result, and the
corresponding taxon.

JSON output
+++++++++++

Single mapping result
~~~~~~~~~~~~~~~~~~~~~

- Taxon:

.. code-block:: json

  {
    "mode" : "mapping",
    "query" : "Influenza B virus (B/Acre/121609/2012)",
    "cast" : "taxon",
    "parentid" : 11520,
    "name" : "Influenza B virus (B/Acre/121609/2012)",
    "taxon" :
    {
      "taxid" : 1334390,
      "rank" : "no rank",
      "names" :
      {
        "Influenza B virus (B/Acre/121609/2012)" : "scientific_name"
      }
    }
  }

- Accession:

.. code-block:: json

  {
    "mode" : "mapping",
    "query" : "ASM1001476v1",
    "cast" : "accs",
    "db":"assembly",
    "uid":5515991,
    "accession" :
    {
      "taxid" : 1962788,
      "accessions" :
      {
        "assemblyaccession" : "GCA_010014765.1",
        "lastmajorreleaseaccession" : "GCA_010014765.1",
        "assemblyname" : "ASM1001476v1"
      }
    }
  }

Multiple mapping results
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: json
  :linenos:

  {"mode":"mapping","query":"Influenza B virus (B/Acre/121609/2012)","cast":"taxon","taxon":{"taxid":1334390,"rank":"no rank","names":{"Influenza B virus (B/Acre/121609/2012)":"scientific_name"},"parentid":11520,"name":"Influenza B virus (B/Acre/121609/2012)"}}
  {"mode":"mapping","query":"man","cast":"taxon","taxon":{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"}}
  {"mode":"mapping","query":"562","cast":"taxon","taxon":{"taxid":562,"rank":"species","names":{"Escherichia coli":"scientific_name","Bacillus coli":"Synonym","Bacterium coli":"Synonym","Bacterium coli commune":"Synonym","Enterococcus coli":"Synonym","E. coli":"CommonName","Escherichia sp. 3_2_53FAA":"Includes","Escherichia sp. MAR":"Includes","bacterium 10a":"Includes","bacterium E3":"Includes","Escherichia/Shigella coli":"EquivalentName","ATCC 11775":"type material","ATCC:11775":"type material","BCCM/LMG:2092":"type material","CCUG 24":"type material","CCUG 29300":"type material","CCUG:24":"type material","CCUG:29300":"type material","CIP 54.8":"type material","CIP:54.8":"type material","DSM 30083":"type material","DSM:30083":"type material","IAM 12119":"type material","IAM:12119":"type material","JCM 1649":"type material","JCM:1649":"type material","LMG 2092":"type material","LMG:2092":"type material","NBRC 102203":"type material","NBRC:102203":"type material","NCCB 54008":"type material","NCCB:54008":"type material","NCTC 9001":"type material","NCTC:9001":"type material","personal::U5/41":"type material","strain U5/41":"type material"},"parentid":561,"name":"Escherichia coli"}}
  {"mode":"mapping","query":"ASM1001476v1","cast":"accs","accession":{"taxid":1962788,"accessions":{"assemblyaccession":"GCA_010014765.1","lastmajorreleaseaccession":"GCA_010014765.1","assemblyname":"ASM1001476v1"},"db":"assembly","uid":5515991}}
  {"mode":"mapping","query":"PRJNA604394","cast":"accs","accession":{"taxid":573,"accessions":{"project_id":604394,"project_acc":"PRJNA604394","project_name":"Klebsiella pneumoniae strain:S01"},"db":"bioproject","uid":604394}}

XML output
++++++++++

Single mapping result
~~~~~~~~~~~~~~~~~~~~~

- Taxon:

.. code-block:: xml

  <mapping>
    <query cast="taxon">man</query>
    <taxon>
      <taxid>9606</taxid>
      <rank>species</rank>
      <name>Homo sapiens</name>
      <parentid>9605</parentid>
      <names>
        <name type="scientific_name">Homo sapiens</name>
        <name type="GenbankCommonName">human</name>
        <name type="CommonName">man</name>
      </names>
    </taxon>
  </mapping>


- Accession:

.. code-block:: xml

  <mapping>
    <query cast="accession">ASM1001476v1</query>
    <accession>
      <taxid>1962788</taxid>
      <uid>5515991</uid>
      <database>assembly</database>
      <accessions>
        <assemblyaccession>GCA_010014765.1</assemblyaccession>
        <lastmajorreleaseaccession>GCA_010014765.1</lastmajorreleaseaccession>
        <assemblyname>ASM1001476v1</assemblyname>
      </accessions>
    </accession>
  </mapping>


Multiple mapping results
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
  :linenos:

  <mapping><query cast="taxon">Influenza B virus (B/Acre/121609/2012)</query><taxon><taxid>1334390</taxid><rank>no rank</rank><name>Influenza B virus (B/Acre/121609/2012)</name><parentid>11520</parentid><names><name type="scientific_name">Influenza B virus (B/Acre/121609/2012)</name></names></taxon></mapping>
  <mapping><query cast="taxon">man</query><taxon><taxid>9606</taxid><rank>species</rank><name>Homo sapiens</name><parentid>9605</parentid><names><name type="scientific_name">Homo sapiens</name><name type="GenbankCommonName">human</name><name type="CommonName">man</name></names></taxon></mapping>
  <mapping><query cast="taxon">562</query><taxon><taxid>562</taxid><rank>species</rank><name>Escherichia coli</name><parentid>561</parentid><names><name type="scientific_name">Escherichia coli</name><name type="Synonym">Bacillus coli</name><name type="Synonym">Bacterium coli</name><name type="Synonym">Bacterium coli commune</name><name type="Synonym">Enterococcus coli</name><name type="CommonName">E. coli</name><name type="Includes">Escherichia sp. 3_2_53FAA</name><name type="Includes">Escherichia sp. MAR</name><name type="Includes">bacterium 10a</name><name type="Includes">bacterium E3</name><name type="EquivalentName">Escherichia/Shigella coli</name><name type="type material">ATCC 11775</name><name type="type material">ATCC:11775</name><name type="type material">BCCM/LMG:2092</name><name type="type material">CCUG 24</name><name type="type material">CCUG 29300</name><name type="type material">CCUG:24</name><name type="type material">CCUG:29300</name><name type="type material">CIP 54.8</name><name type="type material">CIP:54.8</name><name type="type material">DSM 30083</name><name type="type material">DSM:30083</name><name type="type material">IAM 12119</name><name type="type material">IAM:12119</name><name type="type material">JCM 1649</name><name type="type material">JCM:1649</name><name type="type material">LMG 2092</name><name type="type material">LMG:2092</name><name type="type material">NBRC 102203</name><name type="type material">NBRC:102203</name><name type="type material">NCCB 54008</name><name type="type material">NCCB:54008</name><name type="type material">NCTC 9001</name><name type="type material">NCTC:9001</name><name type="type material">personal::U5/41</name><name type="type material">strain U5/41</name></names></taxon></mapping>
  <mapping><query cast="accession">PRJNA604394</query><accession><taxid>573</taxid><uid>604394</uid><database>bioproject</database><accessions><project_id>604394</project_id><project_acc>PRJNA604394</project_acc><project_name>Klebsiella pneumoniae strain:S01</project_name></accessions></accession></mapping>
  <mapping><query cast="accession">ASM1001476v1</query><accession><taxid>1962788</taxid><uid>5515991</uid><database>assembly</database><accessions><assemblyaccession>GCA_010014765.1</assemblyaccession><lastmajorreleaseaccession>GCA_010014765.1</lastmajorreleaseaccession><assemblyname>ASM1001476v1</assemblyname></accessions></accession></mapping>
