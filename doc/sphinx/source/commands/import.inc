.. _importcmd:

Import
======

The ``import`` command import taxa, lineages, and accessions into a local
SQLite database. The import command will print the resulkts from the preceding
command to stanard output.

Local database schema
---------------------

.. code-block:: sql
  :linenos:

  CREATE TABLE taxa
                (id           INTEGER PRIMARY KEY,
                 taxonid     INT NOT NULL,
                 rank         TEXT NULL,
                 parentid    INT NULL,
                 UNIQUE(taxonid));
  CREATE UNIQUE INDEX taxa_idx ON taxa (taxonid);
  CREATE TABLE names
                (id           INTEGER PRIMARY KEY,
                  taxonid     INT,
                  name         TEXT,
                  type         TEXT NULL,
                  FOREIGN KEY (taxonid) REFERENCES taxa(taxonid) ON DELETE CASCADE,
                  UNIQUE(taxonid, name));
  CREATE TRIGGER delete_names DELETE ON names
                BEGIN DELETE FROM names WHERE taxonid=old.taxonid; END;
  CREATE UNIQUE INDEX names_idx ON names (taxonid, name);
  CREATE TABLE accessions
                (id        INTEGER PRIMARY KEY,
                 accession TEXT NOT NULL,
                 db        TEXT NOT NULL,
                 type      TEXT NULL,
                 uid       INT NOT NULL,
                 taxonid  INT NOT NULL,
                 FOREIGN KEY (taxonid) REFERENCES taxa(taxonid) ON DELETE CASCADE,
                 UNIQUE(accession, uid));
  CREATE TRIGGER delete_uids DELETE ON accessions
                BEGIN DELETE FROM accessions WHERE uid=old.uid; END;
  CREATE UNIQUE INDEX accessions_idx ON
                accessions (accession, uid);


Import taxa via ``collect``
---------------------------

.. code-block:: shell

  ncbi-taxonomist  collect -n man -t 2 | ncbi-taxonomist --database taxa-collect.db
  {"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}
  {"taxid":2759,"rank":"superkingdom","names":{"Eukaryota":"scientific_name"},"parentid":131567,"name":"Eukaryota"}
  {"taxid":33154,"rank":"clade","names":{"Opisthokonta":"scientific_name"},"parentid":2759,"name":"Opisthokonta"}
  {"taxid":33208,"rank":"kingdom","names":{"Metazoa":"scientific_name"},"parentid":33154,"name":"Metazoa"}
  {"taxid":6072,"rank":"clade","names":{"Eumetazoa":"scientific_name"},"parentid":33208,"name":"Eumetazoa"}
  {"taxid":33213,"rank":"clade","names":{"Bilateria":"scientific_name"},"parentid":6072,"name":"Bilateria"}
  {"taxid":33511,"rank":"clade","names":{"Deuterostomia":"scientific_name"},"parentid":33213,"name":"Deuterostomia"}
  {"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"}
  {"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"}
  {"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"}
  {"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"}
  {"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"}
  {"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"}
  {"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"}
  {"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"}
  {"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"}
  {"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"}
  {"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"}
  {"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"}
  {"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"}
  {"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"}
  {"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"}
  {"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"}
  {"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"}
  {"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"}
  {"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"}
  {"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"}
  {"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"}
  {"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"}
  {"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"}
  {"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"na     me":"Homo sapiens"}
  {"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}
  {"taxid":2,"rank":"superkingdom","names":{"Bacteria":"scientific_name","eubacteria":"GenbankCommonName","bacteria":"BlastName","Monera":"Inp     art","Procaryotae":"Inpart","Prokaryota":"Inpart","Prokaryotae":"Inpart","prokaryote":"Inpart","prokaryotes":"Inpart"},"parentid":131567,"na     me":"Bacteria"}

- Check database:

.. code-block:: shell

  sqlite3 taxa.db 'SELECT * FROM taxa t JOIN names n on t.taxonid=n.taxonid;'
  id|taxonid|rank|parentid|id|taxonid|name|type
  1|9606|species|9605|1|9606|Homo sapiens|scientific_name
  1|9606|species|9605|2|9606|human|GenbankCommonName
  1|9606|species|9605|3|9606|man|CommonName
  2|9605|genus|207598|4|9605|Homo|scientific_name
  3|207598|subfamily|9604|5|207598|Homininae|scientific_name
  4|9604|family|314295|6|9604|Hominidae|scientific_name
  5|314295|superfamily|9526|7|314295|Hominoidea|scientific_name
  6|9526|parvorder|314293|8|9526|Catarrhini|scientific_name
  7|314293|infraorder|376913|9|314293|Simiiformes|scientific_name
  8|376913|suborder|9443|10|376913|Haplorrhini|scientific_name
  9|9443|order|314146|11|9443|Primates|scientific_name
  10|314146|superorder|1437010|12|314146|Euarchontoglires|scientific_name
  11|1437010|clade|9347|13|1437010|Boreoeutheria|scientific_name
  12|9347|clade|32525|14|9347|Eutheria|scientific_name
  13|32525|clade|40674|15|32525|Theria|scientific_name
  14|40674|class|32524|16|40674|Mammalia|scientific_name
  15|32524|clade|32523|17|32524|Amniota|scientific_name
  16|32523|clade|1338369|18|32523|Tetrapoda|scientific_name
  17|1338369|clade|8287|19|1338369|Dipnotetrapodomorpha|scientific_name
  18|8287|superclass|117571|20|8287|Sarcopterygii|scientific_name
  19|117571|clade|117570|21|117571|Euteleostomi|scientific_name
  20|117570|clade|7776|22|117570|Teleostomi|scientific_name
  21|7776|clade|7742|23|7776|Gnathostomata|scientific_name
  22|7742|clade|89593|24|7742|Vertebrata|scientific_name
  23|89593|subphylum|7711|25|89593|Craniata|scientific_name
  24|7711|phylum|33511|26|7711|Chordata|scientific_name
  25|33511|clade|33213|27|33511|Deuterostomia|scientific_name
  26|33213|clade|6072|28|33213|Bilateria|scientific_name
  27|6072|clade|33208|29|6072|Eumetazoa|scientific_name
  28|33208|kingdom|33154|30|33208|Metazoa|scientific_name
  29|33154|clade|2759|31|33154|Opisthokonta|scientific_name
  30|2759|superkingdom|131567|32|2759|Eukaryota|scientific_name
  31|131567|no rank||33|131567|cellular organisms|scientific_name
  32|2|superkingdom|131567|34|2|Bacteria|scientific_name
  32|2|superkingdom|131567|35|2|eubacteria|GenbankCommonName
  32|2|superkingdom|131567|36|2|bacteria|BlastName
  32|2|superkingdom|131567|37|2|Monera|Inpart
  32|2|superkingdom|131567|38|2|Procaryotae|Inpart
  32|2|superkingdom|131567|39|2|Prokaryota|Inpart
  32|2|superkingdom|131567|40|2|Prokaryotae|Inpart
  32|2|superkingdom|131567|41|2|prokaryote|Inpart
  32|2|superkingdom|131567|42|2|prokaryotes|Inpart

Import taxa via ``resolve``
---------------------------

.. code-block:: shell

  ncbi-taxonomist resolve  -n man -t 2 | ncbi-taxonomist import -db taxa-resolve.db
  {"mode":"resolve","query":"man","cast":"taxon","taxon":{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},"lineage":[{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},{"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"},{"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"},{"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"},{"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"},{"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"},{"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"},{"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"},{"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"},{"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"},{"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"},{"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"},{"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"},{"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"},{"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"},{"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"},{"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"},{"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"},{"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"},{"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"},{"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"},{"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"},{"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"},{"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"},{"taxid":33511,"rank":"clade","names":{"Deuterostomia":"scientific_name"},"parentid":33213,"name":"Deuterostomia"},{"taxid":33213,"rank":"clade","names":{"Bilateria":"scientific_name"},"parentid":6072,"name":"Bilateria"},{"taxid":6072,"rank":"clade","names":{"Eumetazoa":"scientific_name"},"parentid":33208,"name":"Eumetazoa"},{"taxid":33208,"rank":"kingdom","names":{"Metazoa":"scientific_name"},"parentid":33154,"name":"Metazoa"},{"taxid":33154,"rank":"clade","names":{"Opisthokonta":"scientific_name"},"parentid":2759,"name":"Opisthokonta"},{"taxid":2759,"rank":"superkingdom","names":{"Eukaryota":"scientific_name"},"parentid":131567,"name":"Eukaryota"},{"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}]}
  {"mode":"resolve","query":"2","cast":"taxon","taxon":{"taxid":2,"rank":"superkingdom","names":{"Bacteria":"scientific_name","eubacteria":"GenbankCommonName","bacteria":"BlastName","Monera":"Inpart","Procaryotae":"Inpart","Prokaryota":"Inpart","Prokaryotae":"Inpart","prokaryote":"Inpart","prokaryotes":"Inpart"},"parentid":131567,"name":"Bacteria"},"lineage":[{"taxid":2,"rank":"superkingdom","names":{"Bacteria":"scientific_name","eubacteria":"GenbankCommonName","bacteria":"BlastName","Monera":"Inpart","Procaryotae":"Inpart","Prokaryota":"Inpart","Prokaryotae":"Inpart","prokaryote":"Inpart","prokaryotes":"Inpart"},"parentid":131567,"name":"Bacteria"},{"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}]}

- Check database:
  The database should be identical to the database created with the ``collect``
  command above.

.. code-block:: shell

  sqlite3 taxa-resolve.db 'SELECT * FROM taxa t JOIN names n ON t.taxonid=n.taxonid;'

Import accessions
-----------------

  Importing accessions does not inmport only the taxid for the accession, not
  any other taxon metadata.

.. code-block:: shell

  ncbi-taxonomist map --entrezdb protein --accessions  AFR11853 AIA66128.1 | ncbi-taxonomist import -db taxa.db

- Check database:

.. code-block:: shell

  sqlite3 -header taxa.db 'SELECT * FROM accessions a JOIN taxa t ON a.taxonid==t.taxonid;'
  id|accession|db|type|uid|taxonid|id|taxonid|rank|parentid
  1|AIA66128.1|protein|accessionversion|641483259|1239567|33|1239567||
  2|AIA66128|protein|caption|641483259|1239567|33|1239567||
  3|gi|641483259|gb|AIA66128.1||protein|extra|641483259|1239567|33|1239567||
  4|AFR11853.1|protein|accessionversion|403044789|1224525|34|1224525||
  5|AFR11853|protein|caption|403044789|1224525|34|1224525||
  6|gi|403044789|gb|AFR11853.1||protein|extra|403044789|1224525|34|1224525||

To add the missing information, please check :ref:`importaccs` for an extended
command accomplishing this. The following example shows the database after
adding the missing data:

.. code-block:: shell

  sqlite3 -header taxa.db 'SELECT * FROM accessions a JOIN taxa t ON a.taxonid==t.taxonid;'
  id|accession|db|type|uid|taxonid|id|taxonid|rank|parentid
  1|AIA66128.1|protein|accessionversion|641483259|1239567|33|1239567|species|249588
  2|AIA66128|protein|caption|641483259|1239567|33|1239567|species|249588
  3|gi|641483259|gb|AIA66128.1||protein|extra|641483259|1239567|33|1239567|species|249588
  4|AFR11853.1|protein|accessionversion|403044789|1224525|34|1224525|species|35278
  5|AFR11853|protein|caption|403044789|1224525|34|1224525|species|35278
  6|gi|403044789|gb|AFR11853.1||protein|extra|403044789|1224525|34|1224525|species|35278


