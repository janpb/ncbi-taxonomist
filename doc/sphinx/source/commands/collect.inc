.. _collectcmd:

Collect
=======

The ``collect`` command fetches taxa from the Entrez database. If Taxids
or names  sharing parts of the same lineage, these taxa are printed only once.

Output format
-------------

The output describes the collected taxa, one per line. A single taxon
has the following structure, for example chimpanzee (tx9598):

.. code-block:: json

  {
   "taxid" : 9598,
   "rank" : "species",
   "parentid" : 9596,
   "name" : "Pan troglodytes",
   "names" :
   {
    "Pan troglodytes" : "scientific_name",
    "chimpanzee" : "GenbankCommonName"
   }
  }

Collecting taxa for chimpanzee and human:

.. code-block:: shell

  ncbi-taxonomist  collect -n chimpanzee human

JSON output
+++++++++++

.. code-block:: json

  {"taxid":131567,"rank":"no rank","names":{"cellular organisms":"scientific_name"},"parentid":null,"name":"cellular organisms"}
  {"taxid":2759,"rank":"superkingdom","names":{"Eukaryota":"scientific_name"},"parentid":131567,"name":"Eukaryota"}
  {"taxid":33154,"rank":"clade","names":{"Opisthokonta":"scientific_name"},"parentid":2759,"name":"Opisthokonta"}
  {"taxid":33208,"rank":"kingdom","names":{"Metazoa":"scientific_name"},"parentid":33154,"name":"Metazoa"}
  {"taxid":6072,"rank":"clade","names":{"Eumetazoa":"scientific_name"},"parentid":33208,"name":"Eumetazoa"}
  {"taxid":33213,"rank":"clade","names":{"Bilateria":"scientific_name"},"parentid":6072,"name":"Bilateria"}
  {"taxid":33511,"rank":"clade","names":{"Deuterostomia":"scientific_name"},"parentid":33213,"name":"Deuterostomia"}
  {"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"}
  {"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"}
  {"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"}
  {"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"}
  {"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"}
  {"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"}
  {"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"}
  {"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"}
  {"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"}
  {"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"}
  {"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"}
  {"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"}
  {"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"}
  {"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"}
  {"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"}
  {"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"}
  {"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"}
  {"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"}
  {"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"}
  {"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"}
  {"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"}
  {"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"}
  {"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"}
  {"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"}
  {"taxid":9596,"rank":"genus","names":{"Pan":"scientific_name"},"parentid":207598,"name":"Pan"}
  {"taxid":9598,"rank":"species","names":{"Pan troglodytes":"scientific_name","chimpanzee":"GenbankCommonName"},"parentid":9596,"name":"Pan troglodytes"}

XML output
++++++++++

.. code-block:: xml

  <taxon><taxid>131567</taxid><rank>no rank</rank><name>cellular organisms</name><parentid>None</parentid><names><name type="scientific_name">cellular organisms</name></names></taxon>
  <taxon><taxid>2759</taxid><rank>superkingdom</rank><name>Eukaryota</name><parentid>131567</parentid><names><name type="scientific_name">Eukaryota</name></names></taxon>
  <taxon><taxid>33154</taxid><rank>clade</rank><name>Opisthokonta</name><parentid>2759</parentid><names><name type="scientific_name">Opisthokonta</name></names></taxon>
  <taxon><taxid>33208</taxid><rank>kingdom</rank><name>Metazoa</name><parentid>33154</parentid><names><name type="scientific_name">Metazoa</name></names></taxon>
  <taxon><taxid>6072</taxid><rank>clade</rank><name>Eumetazoa</name><parentid>33208</parentid><names><name type="scientific_name">Eumetazoa</name></names></taxon>
  <taxon><taxid>33213</taxid><rank>clade</rank><name>Bilateria</name><parentid>6072</parentid><names><name type="scientific_name">Bilateria</name></names></taxon>
  <taxon><taxid>33511</taxid><rank>clade</rank><name>Deuterostomia</name><parentid>33213</parentid><names><name type="scientific_name">Deuterostomia</name></names></taxon>
  <taxon><taxid>7711</taxid><rank>phylum</rank><name>Chordata</name><parentid>33511</parentid><names><name type="scientific_name">Chordata</name></names></taxon>
  <taxon><taxid>89593</taxid><rank>subphylum</rank><name>Craniata</name><parentid>7711</parentid><names><name type="scientific_name">Craniata</name></names></taxon>
  <taxon><taxid>7742</taxid><rank>clade</rank><name>Vertebrata</name><parentid>89593</parentid><names><name type="scientific_name">Vertebrata</name></names></taxon>
  <taxon><taxid>7776</taxid><rank>clade</rank><name>Gnathostomata</name><parentid>7742</parentid><names><name type="scientific_name">Gnathostomata</name></names></taxon>
  <taxon><taxid>117570</taxid><rank>clade</rank><name>Teleostomi</name><parentid>7776</parentid><names><name type="scientific_name">Teleostomi</name></names></taxon>
  <taxon><taxid>117571</taxid><rank>clade</rank><name>Euteleostomi</name><parentid>117570</parentid><names><name type="scientific_name">Euteleostomi</name></names></taxon>
  <taxon><taxid>8287</taxid><rank>superclass</rank><name>Sarcopterygii</name><parentid>117571</parentid><names><name type="scientific_name">Sarcopterygii</name></names></taxon>
  <taxon><taxid>1338369</taxid><rank>clade</rank><name>Dipnotetrapodomorpha</name><parentid>8287</parentid><names><name type="scientific_name">Dipnotetrapodomorpha</name></names></taxon>
  <taxon><taxid>32523</taxid><rank>clade</rank><name>Tetrapoda</name><parentid>1338369</parentid><names><name type="scientific_name">Tetrapoda</name></names></taxon>
  <taxon><taxid>32524</taxid><rank>clade</rank><name>Amniota</name><parentid>32523</parentid><names><name type="scientific_name">Amniota</name></names></taxon>
  <taxon><taxid>40674</taxid><rank>class</rank><name>Mammalia</name><parentid>32524</parentid><names><name type="scientific_name">Mammalia</name></names></taxon>
  <taxon><taxid>32525</taxid><rank>clade</rank><name>Theria</name><parentid>40674</parentid><names><name type="scientific_name">Theria</name></names></taxon>
  <taxon><taxid>9347</taxid><rank>clade</rank><name>Eutheria</name><parentid>32525</parentid><names><name type="scientific_name">Eutheria</name></names></taxon>
  <taxon><taxid>1437010</taxid><rank>clade</rank><name>Boreoeutheria</name><parentid>9347</parentid><names><name type="scientific_name">Boreoeutheria</name></names></taxon>
  <taxon><taxid>314146</taxid><rank>superorder</rank><name>Euarchontoglires</name><parentid>1437010</parentid><names><name type="scientific_name">Euarchontoglires</name></names></taxon>
  <taxon><taxid>9443</taxid><rank>order</rank><name>Primates</name><parentid>314146</parentid><names><name type="scientific_name">Primates</name></names></taxon>
  <taxon><taxid>376913</taxid><rank>suborder</rank><name>Haplorrhini</name><parentid>9443</parentid><names><name type="scientific_name">Haplorrhini</name></names></taxon>
  <taxon><taxid>314293</taxid><rank>infraorder</rank><name>Simiiformes</name><parentid>376913</parentid><names><name type="scientific_name">Simiiformes</name></names></taxon>
  <taxon><taxid>9526</taxid><rank>parvorder</rank><name>Catarrhini</name><parentid>314293</parentid><names><name type="scientific_name">Catarrhini</name></names></taxon>
  <taxon><taxid>314295</taxid><rank>superfamily</rank><name>Hominoidea</name><parentid>9526</parentid><names><name type="scientific_name">Hominoidea</name></names></taxon>
  <taxon><taxid>9604</taxid><rank>family</rank><name>Hominidae</name><parentid>314295</parentid><names><name type="scientific_name">Hominidae</name></names></taxon>
  <taxon><taxid>207598</taxid><rank>subfamily</rank><name>Homininae</name><parentid>9604</parentid><names><name type="scientific_name">Homininae</name></names></taxon>
  <taxon><taxid>9605</taxid><rank>genus</rank><name>Homo</name><parentid>207598</parentid><names><name type="scientific_name">Homo</name></names></taxon>
  <taxon><taxid>9606</taxid><rank>species</rank><name>Homo sapiens</name><parentid>9605</parentid><names><name type="scientific_name">Homo sapiens</name><name type="GenbankCommonName">human</name><name type="CommonName">man</name></names></taxon>
  <taxon><taxid>9596</taxid><rank>genus</rank><name>Pan</name><parentid>207598</parentid><names><name type="scientific_name">Pan</name></names></taxon>
  <taxon><taxid>9598</taxid><rank>species</rank><name>Pan troglodytes</name><parentid>9596</parentid><names><name type="scientific_name">Pan troglodytes</name><name type="GenbankCommonName">chimpanzee</name></names></taxon>
