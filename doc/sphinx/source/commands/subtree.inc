.. _subtreecmd:

Subtree
=======

``ncbi-taxonomist subtree`` collects taxonomic subsamples for taxids or names
in a local database.

.. note::

  Fetching subtrees remotely form Entrez is in development.

A local database is required, for example:

.. code-block:: shell

  $: ncbi-taxonomist collect -t 142786 9606 | ncbi-taxonomist import -db test.db

Collecting subtrees
--------------------

Between two given ranks
+++++++++++++++++++++++

.. code-block:: shell

  $: ncbi-taxonomist subtree -db test.db -t 142786 9606 --lrank order --hrank phylum
  {"mode":"subtree","query":9606,"subtree":[{"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"},{"taxid":314146,"rank":"superorder","names":{"Euarchontoglires":"scientific_name"},"parentid":1437010,"name":"Euarchontoglires"},{"taxid":1437010,"rank":"clade","names":{"Boreoeutheria":"scientific_name"},"parentid":9347,"name":"Boreoeutheria"},{"taxid":9347,"rank":"clade","names":{"Eutheria":"scientific_name"},"parentid":32525,"name":"Eutheria"},{"taxid":32525,"rank":"clade","names":{"Theria":"scientific_name"},"parentid":40674,"name":"Theria"},{"taxid":40674,"rank":"class","names":{"Mammalia":"scientific_name"},"parentid":32524,"name":"Mammalia"},{"taxid":32524,"rank":"clade","names":{"Amniota":"scientific_name"},"parentid":32523,"name":"Amniota"},{"taxid":32523,"rank":"clade","names":{"Tetrapoda":"scientific_name"},"parentid":1338369,"name":"Tetrapoda"},{"taxid":1338369,"rank":"clade","names":{"Dipnotetrapodomorpha":"scientific_name"},"parentid":8287,"name":"Dipnotetrapodomorpha"},{"taxid":8287,"rank":"superclass","names":{"Sarcopterygii":"scientific_name"},"parentid":117571,"name":"Sarcopterygii"},{"taxid":117571,"rank":"clade","names":{"Euteleostomi":"scientific_name"},"parentid":117570,"name":"Euteleostomi"},{"taxid":117570,"rank":"clade","names":{"Teleostomi":"scientific_name"},"parentid":7776,"name":"Teleostomi"},{"taxid":7776,"rank":"clade","names":{"Gnathostomata":"scientific_name"},"parentid":7742,"name":"Gnathostomata"},{"taxid":7742,"rank":"clade","names":{"Vertebrata":"scientific_name"},"parentid":89593,"name":"Vertebrata"},{"taxid":89593,"rank":"subphylum","names":{"Craniata":"scientific_name"},"parentid":7711,"name":"Craniata"},{"taxid":7711,"rank":"phylum","names":{"Chordata":"scientific_name"},"parentid":33511,"name":"Chordata"}]}
  {"mode":"subtree","query":142786,"subtree":[{"taxid":464095,"rank":"order","names":{"Picornavirales":"scientific_name"},"parentid":2732506,"name":"Picornavirales"},{"taxid":2732506,"rank":"class","names":{"Pisoniviricetes":"scientific_name"},"parentid":2732408,"name":"Pisoniviricetes"},{"taxid":2732408,"rank":"phylum","names":{"Pisuviricota":"scientific_name"},"parentid":2732396,"name":"Pisuviricota"}]}

Collect one specific rank
+++++++++++++++++++++++++

.. code-block:: shell

  $: ncbi-taxonomist subtree -db test.db -t 142786 9606 --rank order
  {"mode":"subtree","query":9606,"subtree":[{"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"}]}
  {"mode":"subtree","query":142786,"subtree":[{"taxid":464095,"rank":"order","names":{"Picornavirales":"scientific_name"},"parentid":2732506,"name":"Picornavirales"}]

Collect from a given rank to root and print XML
+++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

  $: ncbi-taxonomist subtree -x -db test.db -t 142786 9606 --lrank order
  <subtree><query value="9606" cast="taxid" /><tree><taxon><taxid>9443</taxid><rank>order</rank><name>Primates</name><parentid>314146</parentid><names><name type="scientific_name">Primates</name></names></taxon><taxon><taxid>314146</taxid><rank>superorder</rank><name>Euarchontoglires</name><parentid>1437010</parentid><names><name type="scientific_name">Euarchontoglires</name></names></taxon><taxon><taxid>1437010</taxid><rank>clade</rank><name>Boreoeutheria</name><parentid>9347</parentid><names><name type="scientific_name">Boreoeutheria</name></names></taxon><taxon><taxid>9347</taxid><rank>clade</rank><name>Eutheria</name><parentid>32525</parentid><names><name type="scientific_name">Eutheria</name></names></taxon><taxon><taxid>32525</taxid><rank>clade</rank><name>Theria</name><parentid>40674</parentid><names><name type="scientific_name">Theria</name></names></taxon><taxon><taxid>40674</taxid><rank>class</rank><name>Mammalia</name><parentid>32524</parentid><names><name type="scientific_name">Mammalia</name></names></taxon><taxon><taxid>32524</taxid><rank>clade</rank><name>Amniota</name><parentid>32523</parentid><names><name type="scientific_name">Amniota</name></names></taxon><taxon><taxid>32523</taxid><rank>clade</rank><name>Tetrapoda</name><parentid>1338369</parentid><names><name type="scientific_name">Tetrapoda</name></names></taxon><taxon><taxid>1338369</taxid><rank>clade</rank><name>Dipnotetrapodomorpha</name><parentid>8287</parentid><names><name type="scientific_name">Dipnotetrapodomorpha</name></names></taxon><taxon><taxid>8287</taxid><rank>superclass</rank><name>Sarcopterygii</name><parentid>117571</parentid><names><name type="scientific_name">Sarcopterygii</name></names></taxon><taxon><taxid>117571</taxid><rank>clade</rank><name>Euteleostomi</name><parentid>117570</parentid><names><name type="scientific_name">Euteleostomi</name></names></taxon><taxon><taxid>117570</taxid><rank>clade</rank><name>Teleostomi</name><parentid>7776</parentid><names><name type="scientific_name">Teleostomi</name></names></taxon><taxon><taxid>7776</taxid><rank>clade</rank><name>Gnathostomata</name><parentid>7742</parentid><names><name type="scientific_name">Gnathostomata</name></names></taxon><taxon><taxid>7742</taxid><rank>clade</rank><name>Vertebrata</name><parentid>89593</parentid><names><name type="scientific_name">Vertebrata</name></names></taxon><taxon><taxid>89593</taxid><rank>subphylum</rank><name>Craniata</name><parentid>7711</parentid><names><name type="scientific_name">Craniata</name></names></taxon><taxon><taxid>7711</taxid><rank>phylum</rank><name>Chordata</name><parentid>33511</parentid><names><name type="scientific_name">Chordata</name></names></taxon><taxon><taxid>33511</taxid><rank>clade</rank><name>Deuterostomia</name><parentid>33213</parentid><names><name type="scientific_name">Deuterostomia</name></names></taxon><taxon><taxid>33213</taxid><rank>clade</rank><name>Bilateria</name><parentid>6072</parentid><names><name type="scientific_name">Bilateria</name></names></taxon><taxon><taxid>6072</taxid><rank>clade</rank><name>Eumetazoa</name><parentid>33208</parentid><names><name type="scientific_name">Eumetazoa</name></names></taxon><taxon><taxid>33208</taxid><rank>kingdom</rank><name>Metazoa</name><parentid>33154</parentid><names><name type="scientific_name">Metazoa</name></names></taxon><taxon><taxid>33154</taxid><rank>clade</rank><name>Opisthokonta</name><parentid>2759</parentid><names><name type="scientific_name">Opisthokonta</name></names></taxon><taxon><taxid>2759</taxid><rank>superkingdom</rank><name>Eukaryota</name><parentid>131567</parentid><names><name type="scientific_name">Eukaryota</name></names></taxon><taxon><taxid>131567</taxid><rank>no rank</rank><name>cellular organisms</name><parentid>None</parentid><names><name type="scientific_name">cellular organisms</name></names></taxon></tree></subtree>
  <subtree><query value="142786" cast="taxid" /><tree><taxon><taxid>464095</taxid><rank>order</rank><name>Picornavirales</name><parentid>2732506</parentid><names><name type="scientific_name">Picornavirales</name></names></taxon><taxon><taxid>2732506</taxid><rank>class</rank><name>Pisoniviricetes</name><parentid>2732408</parentid><names><name type="scientific_name">Pisoniviricetes</name></names></taxon><taxon><taxid>2732408</taxid><rank>phylum</rank><name>Pisuviricota</name><parentid>2732396</parentid><names><name type="scientific_name">Pisuviricota</name></names></taxon><taxon><taxid>2732396</taxid><rank>kingdom</rank><name>Orthornavirae</name><parentid>2559587</parentid><names><name type="scientific_name">Orthornavirae</name></names></taxon><taxon><taxid>2559587</taxid><rank>clade</rank><name>Riboviria</name><parentid>10239</parentid><names><name type="scientific_name">Riboviria</name></names></taxon><taxon><taxid>10239</taxid><rank>superkingdom</rank><name>Viruses</name><parentid>None</parentid><names><name type="scientific_name">Viruses</name></names></taxon></tree></subtree>

Collect from a given rank to lowest rank
++++++++++++++++++++++++++++++++++++++++

.. code-block:: shell

  $: ncbi-taxonomist subtree  -db test.db -t 142786 9606 --hrank order
  {"mode":"subtree","query":9606,"subtree":[{"taxid":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parentid":9605,"name":"Homo sapiens"},{"taxid":9605,"rank":"genus","names":{"Homo":"scientific_name"},"parentid":207598,"name":"Homo"},{"taxid":207598,"rank":"subfamily","names":{"Homininae":"scientific_name"},"parentid":9604,"name":"Homininae"},{"taxid":9604,"rank":"family","names":{"Hominidae":"scientific_name"},"parentid":314295,"name":"Hominidae"},{"taxid":314295,"rank":"superfamily","names":{"Hominoidea":"scientific_name"},"parentid":9526,"name":"Hominoidea"},{"taxid":9526,"rank":"parvorder","names":{"Catarrhini":"scientific_name"},"parentid":314293,"name":"Catarrhini"},{"taxid":314293,"rank":"infraorder","names":{"Simiiformes":"scientific_name"},"parentid":376913,"name":"Simiiformes"},{"taxid":376913,"rank":"suborder","names":{"Haplorrhini":"scientific_name"},"parentid":9443,"name":"Haplorrhini"},{"taxid":9443,"rank":"order","names":{"Primates":"scientific_name"},"parentid":314146,"name":"Primates"}]}
  {"mode":"subtree","query":142786,"subtree":[{"taxid":142786,"rank":"genus","names":{"Norovirus":"scientific_name","Norwalk-like viruses":"EquivalentName"},"parentid":11974,"name":"Norovirus"},{"taxid":11974,"rank":"family","names":{"Caliciviridae":"scientific_name"},"parentid":464095,"name":"Caliciviridae"},{"taxid":464095,"rank":"order","names":{"Picornavirales":"scientific_name"},"parentid":2732506,"name":"Picornavirales"}]}

Output format
-------------

JSON output
+++++++++++

.. code-block:: json

  {
    "mode": "subtree",
    "query": 9606,
    "subtree": [
      {
        "taxid": 9443,
        "rank": "order",
        "names": {
          "Primates": "scientific_name"
        },
        "parentid": 314146,
        "name": "Primates"
      },
      {
        "taxid": 314146,
        "rank": "superorder",
        "names": {
          "Euarchontoglires": "scientific_name"
        },
        "parentid": 1437010,
        "name": "Euarchontoglires"
      },
      {
        "taxid": 1437010,
        "rank": "clade",
        "names": {
          "Boreoeutheria": "scientific_name"
        },
        "parentid": 9347,
        "name": "Boreoeutheria"
      },
      {
        "taxid": 9347,
        "rank": "clade",
        "names": {
          "Eutheria": "scientific_name"
        },
        "parentid": 32525,
        "name": "Eutheria"
      },
      {
        "taxid": 32525,
        "rank": "clade",
        "names": {
          "Theria": "scientific_name"
        },
        "parentid": 40674,
        "name": "Theria"
      },
      {
        "taxid": 40674,
        "rank": "class",
        "names": {
          "Mammalia": "scientific_name"
        },
        "parentid": 32524,
        "name": "Mammalia"
      },
      {
        "taxid": 32524,
        "rank": "clade",
        "names": {
          "Amniota": "scientific_name"
        },
        "parentid": 32523,
        "name": "Amniota"
      },
      {
        "taxid": 32523,
        "rank": "clade",
        "names": {
          "Tetrapoda": "scientific_name"
        },
        "parentid": 1338369,
        "name": "Tetrapoda"
      },
      {
        "taxid": 1338369,
        "rank": "clade",
        "names": {
          "Dipnotetrapodomorpha": "scientific_name"
        },
        "parentid": 8287,
        "name": "Dipnotetrapodomorpha"
      },
      {
        "taxid": 8287,
        "rank": "superclass",
        "names": {
          "Sarcopterygii": "scientific_name"
        },
        "parentid": 117571,
        "name": "Sarcopterygii"
      },
      {
        "taxid": 117571,
        "rank": "clade",
        "names": {
          "Euteleostomi": "scientific_name"
        },
        "parentid": 117570,
        "name": "Euteleostomi"
      },
      {
        "taxid": 117570,
        "rank": "clade",
        "names": {
          "Teleostomi": "scientific_name"
        },
        "parentid": 7776,
        "name": "Teleostomi"
      },
      {
        "taxid": 7776,
        "rank": "clade",
        "names": {
          "Gnathostomata": "scientific_name"
        },
        "parentid": 7742,
        "name": "Gnathostomata"
      },
      {
        "taxid": 7742,
        "rank": "clade",
        "names": {
          "Vertebrata": "scientific_name"
        },
        "parentid": 89593,
        "name": "Vertebrata"
      },
      {
        "taxid": 89593,
        "rank": "subphylum",
        "names": {
          "Craniata": "scientific_name"
        },
        "parentid": 7711,
        "name": "Craniata"
      },
      {
        "taxid": 7711,
        "rank": "phylum",
        "names": {
          "Chordata": "scientific_name"
        },
        "parentid": 33511,
        "name": "Chordata"
      }
    ]
  }
  {
    "mode": "subtree",
    "query": 142786,
    "subtree": [
      {
        "taxid": 464095,
        "rank": "order",
        "names": {
          "Picornavirales": "scientific_name"
        },
        "parentid": 2732506,
        "name": "Picornavirales"
      },
      {
        "taxid": 2732506,
        "rank": "class",
        "names": {
          "Pisoniviricetes": "scientific_name"
        },
        "parentid": 2732408,
        "name": "Pisoniviricetes"
      },
      {
        "taxid": 2732408,
        "rank": "phylum",
        "names": {
          "Pisuviricota": "scientific_name"
        },
        "parentid": 2732396,
        "name": "Pisuviricota"
      }
    ]
  }


XML output
++++++++++

.. code-block:: xml

  <subtree>
  <query value="9606" cast="taxid" />
  <tree>
    <taxon>
      <taxid>9443</taxid>
      <rank>order</rank>
      <name>Primates</name>
      <parentid>314146</parentid>
      <names>
        <name type="scientific_name">Primates</name>
      </names>
    </taxon>
    <taxon>
      <taxid>314146</taxid>
      <rank>superorder</rank>
      <name>Euarchontoglires</name>
      <parentid>1437010</parentid>
      <names>
        <name type="scientific_name">Euarchontoglires</name>
      </names>
    </taxon>
    <taxon>
      <taxid>1437010</taxid>
      <rank>clade</rank>
      <name>Boreoeutheria</name>
      <parentid>9347</parentid>
      <names>
        <name type="scientific_name">Boreoeutheria</name>
      </names>
    </taxon>
    <taxon>
      <taxid>9347</taxid>
      <rank>clade</rank>
      <name>Eutheria</name>
      <parentid>32525</parentid>
      <names>
        <name type="scientific_name">Eutheria</name>
      </names>
    </taxon>
    <taxon>
      <taxid>32525</taxid>
      <rank>clade</rank>
      <name>Theria</name>
      <parentid>40674</parentid>
      <names>
        <name type="scientific_name">Theria</name>
      </names>
    </taxon>
    <taxon>
      <taxid>40674</taxid>
      <rank>class</rank>
      <name>Mammalia</name>
      <parentid>32524</parentid>
      <names>
        <name type="scientific_name">Mammalia</name>
      </names>
    </taxon>
    <taxon>
      <taxid>32524</taxid>
      <rank>clade</rank>
      <name>Amniota</name>
      <parentid>32523</parentid>
      <names>
        <name type="scientific_name">Amniota</name>
      </names>
    </taxon>
    <taxon>
      <taxid>32523</taxid>
      <rank>clade</rank>
      <name>Tetrapoda</name>
      <parentid>1338369</parentid>
      <names>
        <name type="scientific_name">Tetrapoda</name>
      </names>
    </taxon>
    <taxon>
      <taxid>1338369</taxid>
      <rank>clade</rank>
      <name>Dipnotetrapodomorpha</name>
      <parentid>8287</parentid>
      <names>
        <name type="scientific_name">Dipnotetrapodomorpha</name>
      </names>
    </taxon>
    <taxon>
      <taxid>8287</taxid>
      <rank>superclass</rank>
      <name>Sarcopterygii</name>
      <parentid>117571</parentid>
      <names>
        <name type="scientific_name">Sarcopterygii</name>
      </names>
    </taxon>
    <taxon>
      <taxid>117571</taxid>
      <rank>clade</rank>
      <name>Euteleostomi</name>
      <parentid>117570</parentid>
      <names>
        <name type="scientific_name">Euteleostomi</name>
      </names>
    </taxon>
    <taxon>
      <taxid>117570</taxid>
      <rank>clade</rank>
      <name>Teleostomi</name>
      <parentid>7776</parentid>
      <names>
        <name type="scientific_name">Teleostomi</name>
      </names>
    </taxon>
    <taxon>
      <taxid>7776</taxid>
      <rank>clade</rank>
      <name>Gnathostomata</name>
      <parentid>7742</parentid>
      <names>
        <name type="scientific_name">Gnathostomata</name>
      </names>
    </taxon>
    <taxon>
      <taxid>7742</taxid>
      <rank>clade</rank>
      <name>Vertebrata</name>
      <parentid>89593</parentid>
      <names>
        <name type="scientific_name">Vertebrata</name>
      </names>
    </taxon>
    <taxon>
      <taxid>89593</taxid>
      <rank>subphylum</rank>
      <name>Craniata</name>
      <parentid>7711</parentid>
      <names>
        <name type="scientific_name">Craniata</name>
      </names>
    </taxon>
    <taxon>
      <taxid>7711</taxid>
      <rank>phylum</rank>
      <name>Chordata</name>
      <parentid>33511</parentid>
      <names>
        <name type="scientific_name">Chordata</name>
      </names>
    </taxon>
  </tree>
  </subtree>
