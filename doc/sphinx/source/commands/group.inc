.. _groupcmd:

Group
=====

``ncbi-taxonomist group`` creates and lists taxonomix groups in a local
ncbi-taxonomist database.

Creating a group
----------------

.. code-block:: shell

  $: ncbi-taxonomist collect -n 'Black willow' 'Black hickory' | \
     ncbi-taxonomist import -db taxa.db                        | \
    ncbi-taxonomist group --add tree -db taxa.db


Retrieve a group
----------------

Groups can be retrieved as taxids and processed, e.g. with `jq`, and reused.

.. code-block:: shell

  $: ncbi-taxonomist group --get tree -db taxa.db  | \
     jq '.taxa[]'                                  | \
    ncbi-taxonomist map -t -db taxa.db
