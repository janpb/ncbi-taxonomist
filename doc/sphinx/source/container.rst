.. include:: global.inc
.. _container:

*********
Container
*********

|basename_tt| comes with a Docker container and Singularity image. Both include
``jq`` to facilitate JSON handling.
Both containers have the ``/dbs`` mountpoint to mount host directories, e.g. to
use local databases.

.. contents:: Content
  :local:

.. note::

  The commands shown here assume a current Linux system. Please adjust the
  commands to your system, accordingly.

.. include::  container/docker.inc
.. include::  container/singularity.inc




