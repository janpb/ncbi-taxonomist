.. _entrezresultmod:


Entrez results
==============

Implementations of Entrez results inherited from
:mod:`entrezpy.base.result.EutilsResult`.

Accession result: :mod:`ncbitaxonomist.entrezresult.accession`
--------------------------------------------------------------
.. automodule:: ncbitaxonomist.entrezresult.accession
  :members:
  :undoc-members:


Taxa cache module: :mod:`ncbitaxonomist.entrezresult.mapping`
-------------------------------------------------------------
.. automodule:: ncbitaxonomist.entrezresult.mapping
  :members:
  :undoc-members:

Accession cache module: :mod:`ncbitaxonomist.entrezresult.taxonomy`
-------------------------------------------------------------------
.. automodule:: ncbitaxonomist.entrezresult.taxonomy
  :members:
  :undoc-members:

