.. _logmod:


Logging
=======

Logging  for ``ncbi-taxonomist``.

Configuration: :mod:`ncbitaxonomist.log.conf`
---------------------------------------------
.. automodule:: ncbitaxonomist.log.conf
  :members:
  :undoc-members:


Logger: :mod:`ncbitaxonomist.log.logger`
----------------------------------------
.. automodule:: ncbitaxonomist.log.logger
  :members:
  :undoc-members:

