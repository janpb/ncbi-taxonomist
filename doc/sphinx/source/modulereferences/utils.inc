.. _utils_mod:

Utility functions used across modules
=====================================

Utility functions: :mod:`ncbitaxonomist.utils`
-----------------------------------------------
.. automodule:: ncbitaxonomist.utils
  :members:
  :undoc-members:
