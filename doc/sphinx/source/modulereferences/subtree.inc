.. _subtreemod:

Subtrees
========

Subtrees are selected taxa form lineages.

Subtree
-------

Implemenets a subtree

.. automodule:: ncbitaxonomist.subtree.subtree
  :members:
  :undoc-members:

Subtree analyzer
================

The subtree analyzer manages subtrees

.. automodule:: ncbitaxonomist.subtree.subtreeanalyzer
  :members:
  :undoc-members:

