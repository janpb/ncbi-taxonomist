.. _formattermod:


Formatter
=========

Formats JSON and XML outputs.

Base module: :mod:`ncbitaxonomist.formatter.base`
-------------------------------------------------
.. automodule:: ncbitaxonomist.formatter.base
  :members:
  :undoc-members:


JSON formatter: :mod:`ncbitaxonomist.formatter.jsonformatter`
-------------------------------------------------------------
.. automodule:: ncbitaxonomist.formatter.jsonformatter
  :members:
  :undoc-members:

XML formatter: :mod:`ncbitaxonomist.formatter.xmlformatter`
-----------------------------------------------------------
.. automodule:: ncbitaxonomist.formatter.xmlformatter
  :members:
  :undoc-members:

