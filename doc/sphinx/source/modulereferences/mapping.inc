.. _mapping_mod:

Mappers
=======
Mappers handle the mapping of taxids, names, and accessions to
each other. Analyzers are inherited and adjusted from entrezpy.

Mapper: :mod:`ncbitaxonomist.mapper`
-----------------------------------------
.. automodule:: ncbitaxonomist.mapper
  :members:
  :undoc-members:

Remote mapper: :mod:`ncbitaxonomist.analyzer.mapping`
------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.mapping
  :members:
  :undoc-members:

Remote accession mapper: :mod:`ncbitaxonomist.analyzer.accession`
-------------------------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.accession
  :members:
  :undoc-members:
