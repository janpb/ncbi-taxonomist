.. _parsermod:


Parser
======

Parsers used in ``ncbi-taxonomist``

Argument parser: :mod:`ncbitaxonomist.parser.arguments`
-------------------------------------------------------
.. automodule:: ncbitaxonomist.parser.arguments
  :members:
  :undoc-members:


Group data parser: :mod:`ncbitaxonomist.parser.group`
-----------------------------------------------------
.. automodule:: ncbitaxonomist.parser.group
  :members:
  :undoc-members:

General stdout parser: :mod:`ncbitaxonomist.parser.stdout`
----------------------------------------------------------
.. automodule:: ncbitaxonomist.parser.stdout
  :members:
  :undoc-members:

