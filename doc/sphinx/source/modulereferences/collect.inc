.. _collector_mod:

Collect taxa from Entrez
========================

Collector module: :mod:`ncbitaxonomist.collector`
-------------------------------------------------
.. automodule:: ncbitaxonomist.collector
  :members:
  :undoc-members:
