.. _payloads_mod:

Payloads
========

Payloads implement the requested taxids, names, and accessions. They keep track
what has been successfully analyzed.

Base class for payloads: :mod:`ncbitaxonomist.payload.payload`
--------------------------------------------------------------
.. automodule:: ncbitaxonomist.payload.payload
  :members:
  :undoc-members:

Taxid payload
-------------
.. automodule:: ncbitaxonomist.payload.taxid
  :members:
  :undoc-members:

Names payload
-------------
.. automodule:: ncbitaxonomist.payload.name
  :members:
  :undoc-members:

Accessions payload
------------------
.. automodule:: ncbitaxonomist.payload.accession
  :members:
  :undoc-members:

Accession map payload
---------------------
.. automodule:: ncbitaxonomist.payload.accessionmap
  :members:
  :undoc-members:
