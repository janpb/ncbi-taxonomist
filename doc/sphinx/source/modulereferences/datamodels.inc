.. _models:

Data models
===========

|basename_tt| data models implement taxonomic and accession data. Models use a
:class:`ncbitaxonomist.model.datamodel.DataModel` as base class.

Basic data model: :mod:`ncbitaxonomist.model.datamodel`
--------------------------------------------------------

.. automodule:: ncbitaxonomist.model.datamodel
    :members:
    :undoc-members:

Taxon model: :mod:`ncbitaxonomist.model.taxon`
-----------------------------------------------
.. inheritance-diagram:: ncbitaxonomist.model.taxon
.. automodule:: ncbitaxonomist.model.taxon
    :members:
    :undoc-members:
    :inherited-members:

Accession Data model: :mod:`ncbitaxonomist.model.accession`
------------------------------------------------------------
.. inheritance-diagram:: ncbitaxonomist.model.accession
.. automodule:: ncbitaxonomist.model.accession
    :members:
    :undoc-members:
    :inherited-members:
