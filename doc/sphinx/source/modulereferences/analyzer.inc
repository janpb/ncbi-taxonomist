.. _analyzermod:


Analyzer
========

Analyzer handle remote data from Entrez and are inherited from
:mod:`entrezpy.base.analyzer.EutilsAnalyzer`.

Accession analyzer: :mod:`ncbitaxonomist.analyzer.accession`
------------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.accession
  :members:
  :undoc-members:
  :noindex:


Collection analyzer: :mod:`ncbitaxonomist.analyzer.collect`
-----------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.collect
  :members:
  :undoc-members:

Mapping analyzer: :mod:`ncbitaxonomist.analyzer.mapping`
--------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.mapping
  :members:
  :undoc-members:
  :noindex:

Resolve analyzer: :mod:`ncbitaxonomist.analyzer.resolve`
--------------------------------------------------------
.. automodule:: ncbitaxonomist.analyzer.resolve
  :members:
  :undoc-members:

