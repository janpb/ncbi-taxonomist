.. _resolvermod:

Resolver
========

The resolver module implements the resolving of lineages for names, taxids, and
accessions.

.. automodule:: ncbitaxonomist.resolve.resolver
  :members:
  :undoc-members:

Lineage resolver
================

The lineage resolver resolves whole lineages or the lienage taxa between
given ranks.

.. automodule:: ncbitaxonomist.resolve.lineageresolver
  :members:
  :undoc-members:

