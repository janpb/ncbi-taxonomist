.. _queriesmod:


Queries
=======

Queries are modules implementing a specific taxonomic query, either remote or
for a local database.

Collect queries
---------------

Queries to collect taxa remotely from Entrez.

Base query: :mod:`ncbitaxonomist.query.collect.collect`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.collect.collect
  :members:
  :undoc-members:

Name query: :mod:`ncbitaxonomist.query.collect.name`
++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.collect.name
  :members:
  :undoc-members:

Taxid query: :mod:`ncbitaxonomist.query.collect.taxid`
++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.collect.taxid
  :members:
  :undoc-members:


Map queries
-----------

Queries to map taxa locally or remotely from Entrez.

Base query: :mod:`ncbitaxonomist.query.map.map`
+++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.map.map
  :members:
  :undoc-members:

Name query: :mod:`ncbitaxonomist.query.map.name`
++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.map.name
  :members:
  :undoc-members:

Taxid query: :mod:`ncbitaxonomist.query.map.taxid`
++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.map.taxid
  :members:
  :undoc-members:

Accession query: :mod:`ncbitaxonomist.query.map.accession`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.map.accession
  :members:
  :undoc-members:

Resolve queries
---------------

Queries to resolve taxa and accessions locally or remotely from Entrez.

Base query: :mod:`ncbitaxonomist.query.resolve.resolve`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.resolve.resolve
  :members:
  :undoc-members:

Name query: :mod:`ncbitaxonomist.query.resolve.name`
++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.resolve.name
  :members:
  :undoc-members:

Taxid query: :mod:`ncbitaxonomist.query.resolve.taxid`
++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.resolve.taxid
  :members:
  :undoc-members:

Accession query: :mod:`ncbitaxonomist.query.resolve.accession`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.query.resolve.accession
  :members:
  :undoc-members:

Remote query pipelines
-----------------------

:mod:`entrezpy.conduit` pipelines to fetch remote query data.

.. automodule:: ncbitaxonomist.query.entrez
  :members:
  :undoc-members:

