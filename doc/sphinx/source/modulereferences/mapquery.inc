.. _collectquerymod:


Queries
=======

Queries are modules implementing a specific taxonomic query, either remote or
for a local database.

Collect queries
===============

Queries to collect taxa remotely from Entrez.

Base query: :mod:`ncbitaxonomist.query.collect.collect`
-------------------------------------------------------
.. automodule:: ncbitaxonomist.query.collect.collect
  :members:
  :undoc-members:

Name query: :mod:`ncbitaxonomist.query.collect.name`
----------------------------------------------------
.. automodule:: ncbitaxonomist.query.collect.name
  :members:
  :undoc-members:

Taxid query: :mod:`ncbitaxonomist.query.collect.taxid`
------------------------------------------------------
.. automodule:: ncbitaxonomist.query.collect.taxid
  :members:
  :undoc-members:
