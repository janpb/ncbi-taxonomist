.. _cache_mod:


Cache
=====

Cache caches taxa to reuse already solved queries, avoiding unnessecary local
or remote database lookups.

Cache module: :mod:`ncbitaxonomist.cache`
-----------------------------------------
.. automodule:: ncbitaxonomist.cache
  :members:
  :undoc-members:


Taxa cache module: :mod:`ncbitaxonomist.cache.taxa`
---------------------------------------------------
.. automodule:: ncbitaxonomist.cache.taxa
  :members:
  :undoc-members:

Accession cache module: :mod:`ncbitaxonomist.cache.accession`
-------------------------------------------------------------
.. automodule:: ncbitaxonomist.cache.accession
  :members:
  :undoc-members:

