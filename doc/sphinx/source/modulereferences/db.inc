.. _db_mod:

Database
========

Database modules for a local |basename_tt| database

Database manager: :mod:`ncbitaxonomist.db.dbmanager`
-----------------------------------------------------

.. automodule:: ncbitaxonomist.db.dbmanager
  :members:
  :undoc-members:

Database importer: :mod:`ncbitaxonomist.db.dbimporter`
-------------------------------------------------------
.. automodule:: ncbitaxonomist.db.dbimporter
  :members:
  :undoc-members:

Database tables
---------------

Base table:  :mod:`ncbitaxonomist.db.table.basetable`
++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.db.table.basetable
  :members:
  :undoc-members:

Taxa table: :mod:`ncbitaxonomist.db.table.taxa`
++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.db.table.taxa
  :inherited-members:
  :members:
  :undoc-members:

Names table: :mod:`ncbitaxonomist.db.table.names`
++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.db.table.names
  :inherited-members:
  :members:
  :undoc-members:

Accession table: :mod:`ncbitaxonomist.db.table.accessions`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.db.table.accessions
  :inherited-members:
  :members:
  :undoc-members:

Accession table: :mod:`ncbitaxonomist.db.table.groups`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbitaxonomist.db.table.groups
  :inherited-members:
  :members:
  :undoc-members:
