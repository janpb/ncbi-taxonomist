.. _convertermod:


Converter
=========

Converter convert between data models and pure attributes.

Base converter: :mod:`ncbitaxonomist.convert.converter`
-------------------------------------------------------
.. automodule:: ncbitaxonomist.convert.converter
  :members:
  :undoc-members:

Attribute mapping: :mod:`ncbitaxonomist.convert.convertermap`
-------------------------------------------------------------
Maps indicating which data attributes are convertred to which model attributes.

.. automodule:: ncbitaxonomist.convert.convertermap
  :members:
  :undoc-members:

Local database accession converter: :mod:`ncbitaxonomist.convert.accessiondb`
-----------------------------------------------------------------------------
.. automodule:: ncbitaxonomist.convert.accessiondb
  :members:
  :undoc-members:

NCBI accessions converter: :mod:`ncbitaxonomist.convert.ncbiaccession`
----------------------------------------------------------------------
.. automodule:: ncbitaxonomist.convert.ncbiaccession
  :members:
  :undoc-members:

NCBI taxon converter: :mod:`ncbitaxonomist.convert.ncbitaxon`
-------------------------------------------------------------
.. automodule:: ncbitaxonomist.convert.ncbitaxon
  :members:
  :undoc-members:

Local database taxon converter: :mod:`ncbitaxonomist.convert.taxadb`
--------------------------------------------------------------------
.. automodule:: ncbitaxonomist.convert.taxadb
  :members:
  :undoc-members:



