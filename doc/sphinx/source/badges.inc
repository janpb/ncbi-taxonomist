.. image:: https://img.shields.io/static/v1?label=LICENSE&message=GPLv3+&color=orange&style=for-the-badge&link=https://www.gnu.org/licenses/gpl-3.0.en.html&labelColor=36454f
  :target:  https://www.gnu.org/licenses/gpl-3.0.en.html


.. image:: https://img.shields.io/librariesio/release/pypi/ncbi-taxonomist?style=for-the-badge&label=dependency%3A%3Aentrezpy&logo=pypi&labelColor=36454f&link=https://gitlab.com/ncbipy/entrezpy
  :target: https://gitlab.com/ncbipy/entrezpy


.. image:: https://img.shields.io/pypi/v/ncbi-taxonomist?style=for-the-badge&logo=pypi&link=https://pypi.org/project/ncbi-taxonomist&labelColor=36454f&link=https://pypi.org/project/ncbi-taxonomist
  :target: https://pypi.org/project/ncbi-taxonomist

.. image:: https://img.shields.io/static/v1?label=imgver&message=1.2.0&color=blue&style=for-the-badge&logo=docker&link=https://gitlab.com/janpb/ncbi-taxonomist/container_registry&labelColor=36454f
    :target: https://gitlab.com/janpb/ncbi-taxonomist/container_registry/

.. image:: https://img.shields.io/static/v1?label=singularity%3A%3Aimgver&message=1.2.0&color=blue&style=for-the-badge&&labelColor=36454f&link=https://cloud.sylabs.io/library/jpb/ncbi-taxonomist
    :target: https://cloud.sylabs.io/library/jpb/ncbi-taxonomist

.. image:: https://img.shields.io/pypi/pyversions/ncbi-taxonomist?color=gray&label=%20&logo=python&style=for-the-badge&&labelColor=36454f

.. image:: https://img.shields.io/pypi/status/ncbi-taxonomist?style=for-the-badge&color=informational&logo=pypi&&labelColor=36454f

.. image:: https://img.shields.io/pypi/format/ncbi-taxonomist?style=for-the-badge&color=informational&logo=pypi&labelColor=36454f

.. image:: https://img.shields.io/static/v1?label=+&message=sourcode&color=red&logo=gitlab&style=for-the-badge&labelColor=36454f&link=https://gitlab.com/janpb/ncbi-taxonomist
  :target: https://gitlab.com/janpb/ncbi-taxonomist
