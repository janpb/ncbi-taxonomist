********
Synopsis
********

.. code-block:: shell

  $: pip install ncbi-taxonomist --user
  $: ncbi-taxonomist collect -n human

|basename_tt| handles and manages phylogenetic data available in `NCBI's
<http://www.ncbi.nlm.nih.gov/>`_ `Entrez databases
<https://doi.org/10.1093/nar/gkw1071>`_ .

Functions
=========

  -  :ref:`collectcmd`
        collect taxa from the Entrez Taxonomy database


  -  :ref:`mapcmd`
          map taxids, names, and accessions to related taxonomic information

  -  :ref:`resolvecmd`:
        resolve lineages for taxa (taxid and names) and accessions, e.g.
        sequence or protein

  -  :ref:`importcmd`:
        store obtained results locally in a SQLite databases

  -  :ref:`subtreecmd`:
        extract a whole lineage, or a specific rank, or a range of ranks, from
        a taxid or name

  -  :ref:`groupcmd`:
        create user defined groups for taxa, for example:

    - create a group for all taxa specific for a project
    - group taxa without a phylogenetic relationship, e.g. group all taxa
      representing trees inot a group "trees"

The |basename_tt| commands, e.g. map or import, can be chained together using
pipes to from more complex tasks. For example, to populate a local database
``collect`` will fetch data remotely from Entrez and print it to STDOUT where
``import`` will read ``STDIN`` and populates the local database (see below).

.. code-block:: shell

  ncbi-taxonomist collect -n human | ncbi-taxonomist import -db taxo.db
