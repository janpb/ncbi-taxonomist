.. include:: global.inc
.. _install:

************
Installation
************

.. contents:: Content
  :local:

|basename_tt| is available on `PyPi <https://pypi.org/project/ncbi-taxonomist>`_
via `pip <https://pip.pypa.io/en/stable/>`_. If you use another Python package
manager than ``pip``, please consult its documentation. If you are installing
|basename_tt| on a non-Linux system, consider the propsed methods as guidelines
and adjust as required.

..  important:: Please note

    If some of the proposed commands are unfamiliar to you, don't
    just invoke them but look them up, e.g. in ``man`` pages or search online.
    Should you be unfamiliar with ``pip``, check ``pip -h``

.. note:: Python 3 vs. Python 2

  Due to co-existing Python 2 and Python 3, some installation commands may be
  invoked slighty different. In addition, `development and support for Python 2
  did stop January 2020 <https://www.python.org/doc/sunset-python-2/>`_ and
  should not be used anymore.

  |basename_tt| requires Python >= 3.8. Depending on your OS and/or
  distribution, the default ``pip`` command can install either Python 2 or
  Python 3 packages. Make sure you use ``pip`` for Python 3, e.g. ``pip3`` on
  Ubuntu.

Local pip install (no root required)
====================================

.. code-block:: shell

  $: pip install ncbi-taxonomist --user

On Linux, |basename_tt| will be installed to ``$HOME/.local/bin``. If you cannot
invoke |basename_tt| from the command line, its' likely ``$HOME/.local/bin`` is
not in your ``$PATH`` (check ``echo $PATH``). In such a case, choose
one of the following possibilities:

  - add ``$HOME/.local/bin`` to your ``$PATH``:

      - ``echo "export PATH=${PATH}:$HOME/.local/bin" >> ~/.bashrc``

  - add an ``alias``:

      - see ``man bash`` or https://www.tldp.org/LDP/abs/html/aliases.html

  - use ``$HOME/.local/bin/ncbi-taxonomist`` implicitly

Global pip install (root required)
==================================

.. code-block:: shell

  $: pip install ncbi-taxonomist

|basename_tt| should be now in ``/usr/local/bin`` and in you ``$PATH``.
