.. include:: global.inc
.. _basics:

***************
Basic functions
***************

All |basename_tt| commands have the following underlying structure:

``ncbi-taxonomist <command> <options>``

This section shows the basic usage of |basename_tt|. More complex examples,
inlcuding data extraction with ``jq`` can be found :ref:`here <cookbook>`.

The output is a single JSON object or XML tree per line for each queried taxid,
name, or accessions. The examples show pretty printed single results for
clarity only.

.. contents:: Contents
  :local:

.. include::  commands/collect.inc
.. include::  commands/map.inc
.. include::  commands/resolve.inc
.. include::  commands/import.inc
.. include::  commands/subtree.inc
.. include::  commands/group.inc






