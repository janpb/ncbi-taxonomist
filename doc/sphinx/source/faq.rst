.. include:: global.inc
.. _faq:

##########################
Frequently Asked Questions
##########################

.. contents:: Content
  :local:

openSSL
=======

I'm getting an ``SSL: CERTIFICATE_VERIFY_FAILED`` error
-------------------------------------------------------

If you encounter an SSL error like ``SSL: CERTIFICATE_VERIFY_FAILED``, you may
need to enable the SSL mofule for Python or update the certifications. It
depends on your OS or distribution.

  - Mac OS you need to find run ``Install Certificates.command``, usually found
    in the folder where Python has been installed.

On Linux, you  may need to update the certificates:

  - Debian: run ``update-ca-certificates --fresh`` and export the
    environment variable ``SSL_CERT_DIR=/etc/ssl/certs``.

  - Arch Linux: install the ca-certificates* packages, e.g.
    ``pacman -S ca-certificates*``

It is also possible to update the certificates via ``pip``:
  - run ``pip install --upgrade certifi``

SQLite
======

``sqlite3.OperationalError: near "ON": syntax error`` during import
-------------------------------------------------------------------

|basename_tt| aborts with an error message similar as shown below:


.. code-block:: shell

  Traceback (most recent call last):
    File "/tools/python/3.7.4/bin/ncbi-taxonomist", line 93, in <module>
      main()
    File "/tools/python/3.7.4/bin/ncbi-taxonomist", line 58, in main
      ncbitaxonomist.db.dbimporter.import_stdin(nt.db)
    File "/tools/python/3.7.4/lib/python3.7/site-packages/ncbitaxonomist/db/dbimporter.py", line 95, in import_stdin
      commit(db, taxa, names)
    File "/tools/python/3.7.4/lib/python3.7/site-packages/ncbitaxonomist/db/dbimporter.py", line 34, in commit
      db.add_taxa(taxa)
    File "/tools/python/3.7.4/lib/python3.7/site-packages/ncbitaxonomist/db/dbmanager.py", line 69, in add_taxa
      self.taxa.insert(self.connection, values)
    File "/tools/python/3.7.4/lib/python3.7/site-packages/ncbitaxonomist/db/table/taxa.py", line 39, in insert
      connection.cursor().executemany(stmt, taxavalues)
  sqlite3.OperationalError: near "ON": syntax error


Possible solution 1
+++++++++++++++++++

The taxonomic database uses an old |basename_tt| database scheme. In this case,
you need to rebuild the database using a current version of |basename_tt|.


Possible solution 2
+++++++++++++++++++

This has been reported earlier (:issue:`2`).

|basename_tt| uses a PostgreSQL-style ``UPSERT`` introduced to SQLite 3.24.0.
You need a recent Python version (>= Python 3.8) and SQlite version >= 3.24.0.
You can use the available containers if you can't update Python or SQLite.


If none of these solutions work for you, please open an issue.
