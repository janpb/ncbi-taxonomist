.. include:: global.inc

###########################
|basename_tt| Documentation
###########################
|release| :: |today|

.. toctree::
  :maxdepth: 1
  :caption: Contents:
  :hidden:

  install
  basics
  cookbook
  container
  faq
  modulereferences

.. include::  badges.inc

.. contents:: Content
  :local:

.. include::  synopsis.inc

*****************************
Requirements and Dependencies
*****************************

Requirements
============

- Required: Python >= 3.8

  ``$: python --version``

- Optional:
  To use local databases, `SQLite <https://sqlite.org/index.html>`_ (>= 3.24.0)
  has to be installed. |basename_tt| works without local databases, but needs to
  fetch all data remotely for each query.

  ``$: sqlite3 --version``


Dependencies
============

 |basename_tt| has one dependency:

  - ``entrezpy``: to handle remote requests to NCBI's Entrez databases

    - https://gitlab.com/ncbipy/entrezpy.git
    - https://pypi.org/project/entrezpy/
    - https://doi.org/10.1093/bioinformatics/btz385


This is a library maintained by myself and relies solely on the Python standard
library. Therefore, |basename_tt| is less prone to suffer
`dependency hell <https://en.wikipedia.org/wiki/Dependency_hell>`_.


*******
Contact
*******

To report bugs and/or errors, please open an issue at
https://gitlab.com/ncbi-taxonomist or contact me at: jan.buchmann@mykolab.com.
Of course, feel free to fork the code, improve it, and/or open a pull request.

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
